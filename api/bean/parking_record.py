
import time
import datetime
import math


class ParkingRecord:
    '''
    停车记录类
    请使用关键字参数初始化对象和访问属性
    关键字：
    record_id, # int
    entry_time, # float 时间戳 进场时间
    leave_time, # float 时间戳 离场时间
    cost, # float 实际费用
    licence_plate, # str 车牌
    status, # int 停车状态，0-离场，1-停车中
    remark, # str 备注
    car_type, # int 车型编号
    standard, # int 收费标准编号
    '''

    def __init__(self, **kw):

        self.__record_id = kw.get('record_id')  # id
        self.__entry_time = kw.get('entry_time')  # 进场时间
        self.__leave_time = kw.get('leave_time')  # 离场时间
        self.__cost = kw.get('cost')  # 实际费用
        self.__licence_plate = kw.get('licence_plate')  # 车牌
        self.__status = kw.get('status')  # 停车状态，0-离场，1-停车中
        self.__remark = kw.get('remark')  # 备注
        self.__car_type = kw.get('car_type')  # 车型
        self.__standard = kw.get('standard')  # 收费标准

    def initByTuple(self, record):
        '''
        利用元组初始化对象，顺序需要与注释中列出的顺序一致
        record_id, # int
        entry_time, # float 时间戳 进场时间
        leave_time, # float 时间戳 离场时间
        cost, # float 实际费用
        licence_plate, # str 车牌
        status, # int 停车状态，0-离场，1-停车中
        remark, # str 备注
        car_type, # int 车型编号
        standard, # int 收费标准编号
        '''
        self.__record_id = record[0]  # id
        self.__entry_time = record[1]  # 进场时间
        self.__leave_time = record[2]  # 离场时间
        self.__cost = record[3]  # 实际费用
        self.__licence_plate = record[4]  # 车牌
        self.__status = record[5]  # 停车状态，0-离场，1-停车中
        self.__remark = record[6]  # 备注
        self.__car_type = record[7]  # 车型
        self.__standard = record[8]  # 收费标准

    def printRecord(self):
        '''
        打印所有属性的值，测试用
        '''
        for name, value in vars(self).items():
            print(name.replace('_ParkingRecord__', ''), ":", value)

    def calculate_cost(self, hour_rate: float, leave_time=time.time()) -> float:
        '''
        计算停车费用，计算后，会自动将费用和离场时间（使用当前时间）写入self的属性
        @param hour_rate: 每小时费用（单位：元）
        @return 停车费用，如果属性不合法，则返回-1
        '''
        # 将时间格式转换一下，如果是字符串，则转化为时间戳
        self.__leave_time = self.toTimestamp(leave_time)
        self.__entry_time = self.toTimestamp(self.__entry_time)
        if not(self.__leave_time and self.__entry_time):
            return -1.0

        second = int(self.__leave_time - self.__entry_time)  # 停车时间(单位：秒)
        hour = math.ceil(second / 3600)  # 不足一小时按照一小时计算，向上取整

        cost = hour * hour_rate  # 停车费用
        self.__cost = cost
        return cost

    def toTimestamp(self, timeValue):
        timeStamp = timeValue
        if isinstance(timeStamp, str):
            # 若传入字符串，转换为时间戳
            timeArray = time.strptime(timeStamp, "%Y-%m-%d %H:%M:%S")
            timeStamp = time.mktime(timeArray)
            print(timeStamp)
        elif isinstance(timeStamp, datetime.datetime):
            timeStamp = time.mktime(timeStamp.timetuple())

        return timeStamp

    # ---------------------------------------------------getter and setter------------------------

    # ----- record_id

    @property
    def record_id(self):
        return self.__record_id

    @record_id.setter
    def record_id(self, value):
        self.__record_id = value

    # ----- entry_time
    @property
    def entry_time(self):
        return self.__entry_time

    @entry_time.setter
    def entry_time(self, value):
        value = self.toTimestamp(value)
        self.__entry_time = value

    # ----- leave_time
    @property
    def leave_time(self):
        return self.__leave_time

    @leave_time.setter
    def leave_time(self, value):
        value = self.toTimestamp(value)
        self.__leave_time = value

    # ----- cost

    @property
    def cost(self):
        return self.__cost

    @cost.setter
    def cost(self, value):
        self.__cost = value

    # ----- licence_plate
    @property
    def licence_plate(self):
        return self.__licence_plate

    @licence_plate.setter
    def licence_plate(self, value):
        self.__licence_plate = value

    # ----- status

    @property
    def status(self):
        return self.__status

    @status.setter
    def status(self, value):
        self.__status = value

    # ----- remark
    @property
    def remark(self):
        return self.__remark

    @remark.setter
    def remark(self, value):
        self.__remark = value

    # ----- car_type
    @property
    def car_type(self):
        return self.__car_type

    @car_type.setter
    def car_type(self, value):
        self.__car_type = value

    # ----- standard
    @property
    def standard(self):
        return self.__standard

    @standard.setter
    def standard(self, value):
        self.__standard = value


if __name__ == "__main__":
    a = ParkingRecord(record_id=1)
    a.printRecord()
