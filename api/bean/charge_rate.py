

class ChargeRate:
    '''
    收费标准类
    请使用关键词参数初始化对象和访问属性
    关键字：
    standard_id, # int
    hour_rate, # float 收费标准
    modify_time, # float 时间戳 修改时间
    car_type, # int 车型
    '''

    def __init__(self, **kw):

        self.__standard_id = kw.get('standard_id')  # id
        self.__hour_rate = kw.get('hour_rate')  # 收费标准
        self.__modify_time = kw.get('modify_time')  # 修改时间

    def initByTuple(self, standard):
        '''
        利用元组初始化对象，顺序需要与注释中列出的顺序一致
        standard_id, # int
        hour_rate, # float 收费标准
        modify_time, # float 时间戳 修改时间
        '''
        self.__standard_id = standard[0]  # id
        self.__hour_rate = standard[1]  # 收费标准
        self.__modify_time = standard[2]  # 修改时间

    def printRate(self):
        '''
        打印所有属性的值，测试用
        '''
        for name, value in vars(self).items():
            print(name.replace('_ChargeRate__', ''), ":", value)

    # ---------------------------------------------------getter and setter------------------------

    # ----- standard_id
    @property
    def standard_id(self):
        return self.__standard_id

    @standard_id.setter
    def standard_id(self, value):
        self.__standard_id = value

    # ----- hour_rate
    @property
    def hour_rate(self):
        return self.__hour_rate

    @hour_rate.setter
    def hour_rate(self, value):
        self.__hour_rate = value

    # ----- modify_time
    @property
    def modify_time(self):
        return self.__modify_time

    @modify_time.setter
    def modify_time(self, value):
        self.__modify_time = value

    # ----- car_type
    @property
    def car_type(self):
        return self.__car_type

    @car_type.setter
    def car_type(self, value):
        self.__car_type = value


if __name__ == "__main__":
    a = ChargeRate(id=1)
    a.printRate()
