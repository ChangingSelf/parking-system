'''
生成数据库的测试数据
生成之后就可以不用了

'''
from bean.parking_record import ParkingRecord
import time
import random
import api


def random_plate():
    '''
    随机生成车牌

    车牌号码规则：
    第1位	第2位	第3位	第4位	第5位	第6位	第7位
    省份简称	发证机关代码	号码	号码	号码	号码	号码
    '''
    provinces = ['藏', '川', '鄂', '甘', '赣', '贵', '桂', '黑',
                 '沪', '吉', '冀', '津', '晋', '京', '辽', '鲁',
                 '蒙', '闽', '宁', '青', '琼', '陕', '苏', '皖',
                 '湘', '新', '渝', '豫', '粤', '云', '浙'
                 ]

    numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ]

    alphabeta = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L',
                 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', ]

    alphabeta_numbers = numbers+alphabeta

    plate = random.choice(provinces) + \
        random.choice(alphabeta) + \
        ''.join(random.choices(alphabeta_numbers, k=5))

    return plate


def random_cartype(cartype_list: list = None) -> int:
    '''
    随机生成车型
    @param cartype_list:车型列表，
    '''
    cartype_list = [1, 2] if cartype_list == None else cartype_list  # 设置默认值
    return random.choice(cartype_list)


def random_next_time(last_time: float = time.time(), min_interval: int = 10, max_interval: int = 3600):
    '''
    随机生成下一辆车来的时间
    @param last_time:上一个时间，时间戳
    @param min_interval:最小时间间隔（单位：秒）
    @param max_interval:最大时间间隔（单位：秒）
    '''
    return last_time + random.randint(min_interval, max_interval)


def toTimeStr(timeStamp: float):
    '''
    将时间戳转换为能够赋值给数据库datetime类型的字符串，此处用于测试
    '''
    return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(timeStamp))


def random_parking_record(entry_time=time.time()) -> ParkingRecord:
    '''
    随机生成一条记录
    @param entry_time:设置入场时间，默认为当前时间。出场时间随机生成
    '''
    new_record = ParkingRecord()
    # 随机生成车牌和车型
    new_record.licence_plate = random_plate()
    new_record.car_type = random_cartype()
    # 随机生成进场和出场时间
    new_record.entry_time = entry_time
    new_record.leave_time = random_next_time(
        entry_time,
        min_interval=3600,  # 最低1小时
        max_interval=3600*5  # 最高5小时
    )

    # 设置收费标准 和 实际花费
    cur_standard = api.query_current_standard(
        new_record.car_type)  # 获取当前车型的收费标准
    new_record.standard = cur_standard.standard_id
    hour_rate = cur_standard.hour_rate  # 每小时停车费用
    # 此方法内部已经把费用和离场时间写入new_record对象中了，也可以用变量接收返回值，为了符合习惯，还是手动赋值
    cost = new_record.calculate_cost(
        hour_rate,
        leave_time=new_record.leave_time)
    new_record.cost = cost

    # 设置停车状态
    new_record.status = 0  # 0-该车已经离场，1-该车正在停车场停车

    # 设置备注
    new_record.remark = ''

    return new_record


def random_parking_records(count: int, start_time: float, min_interval: int = 10, max_interval: int = 3600) -> list:
    '''
    随机生成count条停车时间在start_time到end_time之间的停车信息，返回一个列表
    @param count:停车信息数目
    @param start_time:起始时间
    @param min_interval: 前一辆车开进停车场后，下一辆车进入的最小时间间隔（单位：秒）
    @param max_interval: 前一辆车开进停车场后，下一辆车进入的最大时间间隔（单位：秒）
    @return 元素为随机停车信息的列表
    '''
    record_list = []
    entry_time = start_time
    for i in range(count):
        interval = random.randint(min_interval, max_interval)

        new_record = random_parking_record(entry_time=entry_time)
        # new_record.printRecord()  # 打印出来用于测试
        # print('==============')
        record_list.append(new_record)
        # 随机生成下一辆车的进场时间
        entry_time = random_next_time(
            last_time=entry_time,
            min_interval=min_interval,
            max_interval=max_interval
        )
        # 测试用打印，每生成一条就打印一条
        print('第{}条:'.format(i+1), (new_record.licence_plate, new_record.car_type, new_record.remark,
                                    api.toTimeStr(new_record.entry_time),
                                    api.toTimeStr(new_record.leave_time),
                                    new_record.status,
                                    new_record.standard, new_record.cost))

    return record_list


def insert_records(record_list: list) -> int:
    '''
    将停车记录列表插入到数据库
    @param record_list: 一个列表，元素为ParkingRecord类型
    @return 影响行数
    '''
    sql = 'INSERT INTO record(licence_plate,type,remark,entry_time,leave_time,status,standard,cost) VAlUES(%s,%s,%s,%s,%s,%s,%s,%s)'
    datas = [(record.licence_plate, record.car_type, record.remark,
              api.toTimeStr(record.entry_time),
              api.toTimeStr(record.leave_time),
              record.status,
              record.standard, record.cost) for record in record_list]
    # for data in datas:
    #     print(data)
    return api.database.updateBatch(sql, datas)


if __name__ == "__main__":
    # plate = random_plate()
    # print(plate)
    # car_type = random_cartype()
    # print(car_type)
    # nextTime = random_next_time()
    # print(toTimeStr(nextTime))
    # nextTime += 3600
    # nextTime = random_next_time(nextTime)
    # print(toTimeStr(nextTime))

    # 生成前几年的数据，3600*24*365*3是从3年前开始生成。
    # 这个是秒数，所以3600代表一小时，3600*24为一天，3600*24*365是一年
    start_time = time.time()-3600*24*365*3
    record_list = random_parking_records(40000, start_time=start_time)
    row_count = insert_records(record_list)
    print(row_count)
