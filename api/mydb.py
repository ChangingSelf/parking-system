import pymysql


class MyDB:
    '''
    自定义数据库类，用于操作数据库
    '''

    def __init__(self, **config):
        self.conn = pymysql.connect(**config)
        self.cursor = self.conn.cursor()
        pass

    def query(self, sql):
        '''
        执行查询语句
        @return 结果集
        '''
        self.cursor.execute(sql)
        values = self.cursor.fetchall()
        return values

    def update(self, sql, value=None) -> (int, int):
        '''
        执行更新语句
        @return (影响行数,自增id)
        '''
        if value == None:
            self.cursor.execute(sql)
        else:
            self.cursor.execute(sql, value)

        insert_id = self.cursor.lastrowid  # 要在commit之前取得
        self.conn.commit()
        return self.cursor.rowcount, insert_id

    def updateBatch(self, sql, value_list):
        '''
        执行批量插入
        @return 影响行数
        '''
        self.cursor.executemany(sql, value_list)
        self.conn.commit()
        return self.cursor.rowcount

    def __del__(self):
        '''
        析构函数
        '''
        if self.conn:
            self.conn.close()
