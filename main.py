from PyQt5 import QtCore

from api import api
from MainWindows import Ui_MainWindow
# import cv2
import sys
import os
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
import qdarkstyle

class MyMainClass(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super(MyMainClass, self).__init__(parent)

        self.setupUi(self)
        self.pushButtonIn.clicked.connect(self.In)
        self.actionIn.triggered.connect(self.anaIn)
        self.actionOut.triggered.connect(self.anaOut)


        self.tableWidgetPark.setColumnCount(7)
        self.tableWidgetPark.setRowCount(4)
        self.tableWidgetPark.verticalHeader().setVisible(False)
        self.tableWidgetPark.setHorizontalHeaderLabels(['车牌', '车型', '入库时间', '出库时间', '收费标准', '收费', '备注'])
        # newItem = QTableWidgetItem('张三')
        # self.tableWidgetPark.setItem(0, 0, newItem)

        self.setTableParkAll()


    def In(self):
        print("IN")
        # api.enter_parkinglots('陕B777777', 1, '')

    def anaIn(self):
        print("anaIn")
        fileName, fileType = QFileDialog.getOpenFileName(self, "选取文件", os.getcwd(),
                                                                      "All Files(*);;Text Files(*.txt)")
        if fileName == "":
            QMessageBox.information(self, '提示', '文件为空，请重新操作')

        else:
            print(fileName)
            jpg = QPixmap(fileName).scaled(self.inLabel.width(), self.inLabel.height())
            self.inLabel.setPixmap(jpg)
            


    def anaOut(self):
        print("anaOut")

    def typeChage(self, type):
        if type == 1:
            return "小型车"
        elif type == 2:
            return "大型车"
        else:
            return "电动车"

    def setInBox(self):
        pass

    def setTableParkAll(self):
        self.tableWidgetPark.clear()
        all = api.get_history_record()
        self.tableWidgetPark.setRowCount(len(all))
        self.tableWidgetPark.setHorizontalHeaderLabels(['车牌', '车型', '入库时间', '出库时间', '收费标准', '收费', '备注'])
        i = 0
        for record in all:
            self.tableWidgetPark.setItem(i, 0, QTableWidgetItem(record.licence_plate))
            self.tableWidgetPark.setItem(i, 1, QTableWidgetItem(self.typeChage(record.car_type)))
            self.tableWidgetPark.setItem(i, 2, QTableWidgetItem(record.entry_time.strftime('%Y-%m-%d %H:%M:%S')))
            if record.leave_time != None:
                self.tableWidgetPark.setItem(i, 3, QTableWidgetItem(record.leave_time.strftime('%Y-%m-%d %H:%M:%S')))
            self.tableWidgetPark.setItem(i, 4, QTableWidgetItem(str(record.standard)))
            if record.cost != None:
                self.tableWidgetPark.setItem(i, 5, QTableWidgetItem(str(record.cost)))
            i += 1


if __name__ == '__main__':
    QtCore.QCoreApplication.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling)
    app = QApplication(sys.argv)
    app.setStyleSheet(qdarkstyle.load_stylesheet_pyqt5())
    MainWindow = QMainWindow()
    ui = MyMainClass()
    ui.show()
    sys.exit(app.exec_())