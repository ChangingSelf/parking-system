/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 50173
 Source Host           : localhost:3306
 Source Schema         : parking

 Target Server Type    : MySQL
 Target Server Version : 50173
 File Encoding         : 65001

 Date: 22/10/2020 20:17:46
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for record
-- ----------------------------
DROP TABLE IF EXISTS `record`;
CREATE TABLE `record`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `entry_time` datetime NULL DEFAULT NULL COMMENT '进场时间',
  `leave_time` datetime NULL DEFAULT NULL COMMENT '离场时间',
  `cost` float(11, 2) NULL DEFAULT NULL COMMENT '实际费用',
  `licence_plate` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '车牌',
  `status` smallint(6) NULL DEFAULT NULL COMMENT '停车状态，0-已离场，1-正在停车',
  `remark` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `type` int(11) NULL DEFAULT NULL COMMENT '车型，外键',
  `standard` int(11) NULL DEFAULT NULL COMMENT '该车停车时使用的收费标准，不一定是当前收费标准',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK_Reference_1`(`type`) USING BTREE,
  INDEX `FK_Reference_2`(`standard`) USING BTREE,
  CONSTRAINT `FK_Reference_1` FOREIGN KEY (`type`) REFERENCES `type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_Reference_2` FOREIGN KEY (`standard`) REFERENCES `standard` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 60001 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '停车记录\r\n' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for standard
-- ----------------------------
DROP TABLE IF EXISTS `standard`;
CREATE TABLE `standard`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `hour_rate` float(11, 2) NULL DEFAULT NULL COMMENT '每小时收费（单位：元）',
  `modify_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `type` int(11) NULL DEFAULT NULL COMMENT '修改前对应的车型',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `type`(`type`) USING BTREE,
  CONSTRAINT `type` FOREIGN KEY (`type`) REFERENCES `type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '收费标准\r\n' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of standard
-- ----------------------------
INSERT INTO `standard` VALUES (1, 2.00, '2020-10-09 11:45:30', NULL);
INSERT INTO `standard` VALUES (2, 3.00, '2020-10-09 11:45:41', NULL);
INSERT INTO `standard` VALUES (4, 10.00, '2020-10-12 16:22:21', NULL);
INSERT INTO `standard` VALUES (5, 10.00, '2020-10-12 16:29:11', NULL);
INSERT INTO `standard` VALUES (6, 3.00, '2020-10-22 20:06:56', NULL);
INSERT INTO `standard` VALUES (7, 10.00, '2020-10-22 20:06:56', NULL);
INSERT INTO `standard` VALUES (8, 2.60, '2020-10-22 20:07:14', NULL);
INSERT INTO `standard` VALUES (9, 10.50, '2020-10-22 20:07:14', NULL);
INSERT INTO `standard` VALUES (10, 2.00, '2020-10-22 20:13:38', 1);

-- ----------------------------
-- Table structure for type
-- ----------------------------
DROP TABLE IF EXISTS `type`;
CREATE TABLE `type`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '车型名称，例如“小型车”',
  `standard` int(11) NULL DEFAULT NULL COMMENT '该车型现行标准',
  `carport` int(11) NOT NULL COMMENT '该车型车位数',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK_Reference_3`(`standard`) USING BTREE,
  CONSTRAINT `FK_Reference_3` FOREIGN KEY (`standard`) REFERENCES `standard` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '车型' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of type
-- ----------------------------
INSERT INTO `type` VALUES (1, '小型车', 10, 200);
INSERT INTO `type` VALUES (2, '大型车', 9, 100);
INSERT INTO `type` VALUES (3, '新能源', 5, 500);

SET FOREIGN_KEY_CHECKS = 1;
